# A04

A shared project focusing on ASP.NET 5 server-side views and client-side interfaces. 

## Contributors

Northwest MS-ACS Students in 44-663 

## Getting Started

Required:  Git For Windows  installed.  Your user name and email must be configured (see the directions on the site). 

Recommended: TortoiseGit installed.

Clone the code for A04 from:  [https://bitbucket.org/NW-44-663-2016/a04](https://bitbucket.org/NW-44-663-2016/a04)

We only clone once, at the beginning, to get the code for the first time. 

## Resources 

The entire Pro Git book, written by Scott Chacon and Ben Straub and published by Apress, is available online.

[https://git-scm.com/book/en/v2](https://git-scm.com/book/en/v2)

## Suggestions


If you can't push, it's likely because you don't have the most recent changes.  If this happens, you'll have to:

* commit - or stash - your local changes first
* pull the most recent code from BitBucket
* git merge your code with the new code
* commit and then push to BitBucket something that reflects both your work AND the newest shared code in the shared repo.

Before you begin working, always pull the newest code. The further behind your local version is, the more merges you'll have to make.