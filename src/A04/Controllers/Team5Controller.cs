﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace A04.Controllers
{
    public class Team5Controller : Controller
    {
        // GET: /Team/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Ashishpandey()
        {
            ViewData["Title"] = "Ashish Pandey's View Page";

            return View();
        }

        public IActionResult NadeemAnsari()
        {
            ViewData["Title"] = "Nadeem Ansari's View Page";
            return View();
        }

        public IActionResult SupremRaju()
        {
            ViewData["Title"] = "Suprem Raju Manthena's View Page";
            return View();
        }

        public IActionResult Vijay()
        {
            ViewData["Title"] = "Vijay Gopi's Information.";
            return View();
        }

        public IActionResult Sharanya()
        {
            ViewData["Title"] = "Welcome to  My profile";
            return View();
        }
    }
}
