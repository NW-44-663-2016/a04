
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace A04.Controllers
{
    public class TeamController : Controller
    {
        // GET: /Team/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Denisecase()
        {
            ViewData["Title"] = "Denise Case's View Page";
            ViewData["Protip"] = "Use CTRL-5 to save and refresh (rather than restarting).";

            return View();
        }

        public IActionResult KrishnaPainuly()
        {
            return View();
        }
     
        public IActionResult Manasabojja()
        {
            ViewData["Title"] = "Manasa Bojja's Page";

            return View();
        }
    }

}