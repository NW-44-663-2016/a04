﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace A04.Controllers
{
    public class Team7Controller : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult pradeep()
        {
            ViewData["Title"] = "Pradeep Nandyala's View Page";
            //ViewData["Protip"] = "Use CTRL-5 to save and refresh (rather than restarting).";

            return View();
        }
        public IActionResult Priyanka()
        {
            ViewData["Title"] = "Priyanka Kommula's View Page";
            return View();
        }

        public IActionResult Rahul()
        {
            ViewData["Title"] = "Rahul's View Page";
            return View();
        }
        //Manoj's View Action Result
        public IActionResult manoj()
        {
            ViewData["Title"] = "Manoj Kumar Are";
            return View();

        }
        public IActionResult Preethi()
        {
            ViewData["Title"] = "Preethi's View Page";
            return View();
        }
        public IActionResult Srikanth()
        {
            ViewData["Title"] = "Srikanth's View Page";
            return View();
        }
    }
}
