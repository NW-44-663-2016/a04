﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace A04.Controllers
{
    public class Team6Controller : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult MohammedSardar()
        {

            ViewData["Title"] = "My view page...";

            return View();
        }
        public IActionResult Satvik()
        {
            ViewData["Title"] = "Team-6";
            ViewData["Protip"] = "Bhanu Satvik Reddy";

            return View();
        }
        public IActionResult Rohan()
        {
            ViewData["Title"] = "Team-6";
            ViewData["Protip"] = "Rohanreddy Thummala";

            return View();
        }
        public IActionResult Kiran()
        {
            ViewData["Title"] = "Team-6";
            ViewData["Protip"] = "Kiran Kumar";

            return View();
        }
        public IActionResult Prateek()
        {
            ViewData["Title"] = "Team-6";
            ViewData["Protip"] = "Ravilla Prateek";

            return View();
        }
        public IActionResult TarakeshGogi()
        {

            ViewData["Title"] = "My view!";

            return View();
        }
    }
}