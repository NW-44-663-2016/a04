
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace A04.Controllers
{
    public class Team02Controller : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult BhavanaPilli()
        {
            ViewData["Title"] = "Team 2";
            ViewData["Protip"] = "Bhavana Pilli";

            return View();
        }
        public IActionResult RameshPasupuleti()
        {
            ViewData["Title"] = "Team 2";
            ViewData["Protip"] = "Ramesh Pasupuleti";
            return View();
        }
        public IActionResult KoppuNagaPrasad()
        {
            return View();
        }
        public IActionResult Imaduddin()
        {
            return View();
        }


        public IActionResult SowmyaLakkireddy()
        {
            ViewData["Title"] = "Team 2";
            ViewData["Protip"] = "Sowmya Lakkireddy";


            return View();
        }

    }
}

