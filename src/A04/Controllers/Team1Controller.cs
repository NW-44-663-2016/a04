﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace A04.Controllers
{
    public class Team1Controller : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult AnveshkumarKolluri()
        {
            return View();
        }
        public IActionResult ManindraNareshKumar()
        {
            ViewData["Title"] = "About Manindra Naresh Kumar";
            return View();
        }
        public IActionResult Saikrishna()
        {
            return View();
        }

        public IActionResult Jagadeesh()
        {
            ViewData["Title"] = "About Jagadeesh Basimsetty";
            return View();
        }

          public IActionResult ChandrakanthPodishetty()
        {

            return View();
        }
        public IActionResult RaihanAhmed()
        {
            ViewData["Title"] = "Raihan Ahmed's View Page";
            return View();
        }

    }
}
