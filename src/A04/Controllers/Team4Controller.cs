

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace A04.Controllers
{
    public class Team4Controller : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult SatyaRadhaAddala()
        {
            ViewData["Title"] = "Team-4";
            ViewData["Protip"] = "Satya Radha Addala";
            return View();
        }

        public IActionResult Sandeep()
        {
            ViewData["Title"] = "Team-4";
            ViewData["Protip"] = "Sandeep Varma Rudraraju";
            return View();
        }
        public IActionResult Ramya()
        {
            ViewData["Title"] = "Team-4";
            ViewData["Protip"] = "Ramya V Sankara";
            return View();
        }
        public IActionResult Vamshinath()
        {
            ViewData["Title"] = "Team-4";
            ViewData["Protip"] = "Vamshinath";
            return View();
        }
        public IActionResult Kowshik()
        {
            ViewData["Title"] = "Team-4";
            ViewData["Protip"] = "Kowshik Morishetty";
            return View();
        }
    }
}
