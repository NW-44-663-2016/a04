

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace A04.Controllers
{
    public class Team3 : Controller
    {
        public IActionResult Pranay()
        {
            ViewData["Title"] = "Pranay";
            return View();
        }

        public IActionResult VeerapratapPutta()
        {
            ViewData["Title"] = "Welcome to Team 03..!!!";
            ViewData["Protip"] = "Not All Who Wander Are Lost";
            return View();
        }
        public IActionResult Abhinaya()
        {
            ViewData["Title"] = "Welcome to Team 03..!!!";
            return View();
        }
        public IActionResult SaiNaveen()
        {
            ViewData["Title"] = "Welcome to Team 03..!!!";
            return View();
        }

        public IActionResult Mani()
        {
            return View();
        }
    }
}
