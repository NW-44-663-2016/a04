﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace A04.Controllers
{
    public class Team03 : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "Welcome to Team 03..!!!";
            return View();
        }

        public IActionResult VeerapratapPutta()
        {
            ViewData["Title"] = "Welcome to Team 03..!!!";
            ViewData["Protip"] = "Not All Who Wander Are Lost";
            return View();
        }
        public IActionResult Abhinaya()
        {
            ViewData["Title"] = "Welcome to Team 03..!!!";
            return View();
        }

    }
}
